package com.celestialapps.cookbook.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CookingAlgorithmRequest {

    private String imgUrl;
    private String description;
}
