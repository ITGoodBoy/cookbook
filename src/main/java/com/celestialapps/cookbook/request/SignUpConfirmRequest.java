package com.celestialapps.cookbook.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignUpConfirmRequest {

    private String email;
    private String code;
}
