package com.celestialapps.cookbook.request;

import com.celestialapps.cookbook.service.recipe.Recipe;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecipeRequest {


    private String name;
    private String categoryName;
    private String imgUrl;
    private boolean isPublished;
    private List<IngredientRequest> ingredientRequests;
    private List<CookingAlgorithmRequest> cookingAlgorithmRequests;
    private String videoId;



}
