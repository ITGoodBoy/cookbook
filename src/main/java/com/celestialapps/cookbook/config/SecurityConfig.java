package com.celestialapps.cookbook.config;

import com.celestialapps.cookbook.config.language.LanguageAuthenticationFilter;
import com.celestialapps.cookbook.service.user.UserService;
import com.celestialapps.cookbook.service.user.auth.Role;
import com.celestialapps.cookbook.service.user.auth.token.Token;
import com.celestialapps.cookbook.service.user.auth.token.TokenAuthenticationFilter;
import com.celestialapps.cookbook.service.user.auth.token.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

   // private final UserService userService;
    private final TokenService tokenService;

    @Autowired
    public SecurityConfig(TokenService tokenService) {
        this.tokenService = tokenService;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .headers()
                .cacheControl()
                .disable()
                .and()
                .cors()
                .and()
                .csrf()
                .disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/v2/api-docs/**", "/configuration/ui/**", "/swagger-resources/**",
                        "/configuration/security/**", "/swagger-ui.html**", "/swagger-ui.html", "/webjars/**",
                        "/api/recipes/", "/api/auth/registration", "/api/auth/login")
                .permitAll()
                .and()
                .addFilterBefore(new LanguageAuthenticationFilter(), BasicAuthenticationFilter.class)
                .addFilterBefore(new TokenAuthenticationFilter(tokenService),
                        BasicAuthenticationFilter.class)
                .authorizeRequests();

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
