package com.celestialapps.cookbook.config.language;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Locale;

public class LanguageAuthenticationFilter extends GenericFilterBean {


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;

        //extract language from header
        final String language = httpRequest.getHeader(HttpHeaders.ACCEPT_LANGUAGE);

        if (language != null) {
            Locale locale = new Locale(language);
            LocaleContextHolder.setLocale(locale);
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
