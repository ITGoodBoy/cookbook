package com.celestialapps.cookbook.response;


import com.celestialapps.cookbook.service.cooking_algorithm.CookingAlgorithm;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Sergey on 07.03.2018.
 */
@Data
@NoArgsConstructor
public class RecipeResponse {


    private long categoryId;
    private long id;
    private String videoId;
    private long likeCount;
    private int commentCount;
    private String name;
    private String author;
    private Date date;
    private String imgUrl;
    private boolean isVegetarian;
    private boolean isVegan;
    private boolean isPublished;
    private List<IngredientResponse> ingredientResponses;
    private List<CookingAlgorithmResponse> cookingAlgorithmResponses;



    public RecipeResponse(long categoryId, long id, String videoId, long likeCount, int commentCount,
                          String name, String author, Date date, String imgUrl,
                          boolean isVegetarian, boolean isVegan, boolean isPublished) {
        this.categoryId = categoryId;
        this.id = id;
        this.videoId = videoId;
        this.likeCount = likeCount;
        this.commentCount = commentCount;
        this.name = name;
        this.author = author;
        this.date = date;
        this.imgUrl = imgUrl;
        this.isVegetarian = isVegetarian;
        this.isVegan = isVegan;
        this.isPublished = isPublished;

        this.ingredientResponses = new ArrayList<>();
        this.cookingAlgorithmResponses = new ArrayList<>();
    }

}
