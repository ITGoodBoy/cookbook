package com.celestialapps.cookbook.response;

import com.celestialapps.cookbook.service.comment.Comment;
import com.celestialapps.cookbook.service.cooking_algorithm.CookingAlgorithm;
import com.celestialapps.cookbook.service.ingredient.ingredient.Ingredient;
import com.celestialapps.cookbook.service.recipe.Recipe;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RecipeFullResponse {

    private long id;
    private String name;
    private String categoryName;
    private String imgUrl;
    private long likeCount;
    private String author;
    private String videoId;
    private List<IngredientResponse> ingredientResponses;
    private List<CommentResponse> comments;
    private List<CookingAlgorithmResponse> cookingAlgorithmResponses;
    private Date date;

    public RecipeFullResponse(Recipe recipe) {
        this.id = recipe.getId();
        this.name = recipe.getName();
        this.categoryName = recipe.getCategory().getName();
        this.imgUrl = recipe.getImgUrl();
        this.likeCount = recipe.getLikeCount();
        this.author = recipe.getUser().getLogin();
        this.videoId = recipe.getVideoId();
        this.date = recipe.getDate();

        this.ingredientResponses = recipe
                .getIngredients()
                .stream()
                .map(IngredientResponse::new)
                .collect(Collectors.toList());

        this.comments = recipe
                .getComments()
                .stream()
                .map(CommentResponse::new)
                .collect(Collectors.toList());

        this.cookingAlgorithmResponses = recipe
                .getCookingAlgorithms()
                .stream()
                .map(CookingAlgorithmResponse::new)
                .collect(Collectors.toList());
    }
}
