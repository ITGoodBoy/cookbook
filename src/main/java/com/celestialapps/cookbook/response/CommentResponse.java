package com.celestialapps.cookbook.response;

import com.celestialapps.cookbook.service.comment.Comment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentResponse {

    private long id;
    private String text;
    private Date date;
    private long likeCount;
    private String author;
    private List<CommentResponse> commentResponses;

    public CommentResponse(Comment comment) {
        this.id = comment.getId();
        this.text = comment.getText();
        this.date = comment.getDate();
        this.likeCount = comment.getLikeCount();
        this.author = comment.getUser().getLogin();

        this.commentResponses = comment.getComments().stream().map(CommentResponse::new).collect(Collectors.toList());
    }
}
