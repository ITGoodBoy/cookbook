package com.celestialapps.cookbook.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AllDataResponse {

    private List<CategoryResponse> categoryResponses;
    private List<IngredientShortResponse> ingredientShortResponses;
    private List<RecipeResponse> recipeResponses;
}
