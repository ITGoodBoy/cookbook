package com.celestialapps.cookbook.response;

import com.celestialapps.cookbook.service.cooking_algorithm.CookingAlgorithm;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@AllArgsConstructor
public class CookingAlgorithmResponse {

    @JsonIgnore
    private long recipeId;
    private long id;
    private String text;
    private String imgUrl;

    public CookingAlgorithmResponse(CookingAlgorithm cookingAlgorithm) {
        this.id = cookingAlgorithm.getId();
        this.text = cookingAlgorithm.getText();
        this.imgUrl = cookingAlgorithm.getImgUrl();
    }
}
