package com.celestialapps.cookbook.service.comment;

import com.celestialapps.cookbook.service.user.User;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    private String text;
    private Date date = new Date(System.currentTimeMillis());
    private long likeCount;
    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Comment> comments;

    public Comment(String text, User user, List<Comment> comments) {
        this.text = text;
        this.user = user;
        this.comments = comments;
    }
}
