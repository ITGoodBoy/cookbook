package com.celestialapps.cookbook.service.user.auth.provider;

import com.celestialapps.cookbook.service.user.UserRepository;
import com.celestialapps.cookbook.service.user.auth.token.Token;
import com.celestialapps.cookbook.service.user.auth.token.TokenService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;

import java.util.Optional;

@Slf4j
class ProviderFacebook extends Provider{

    ProviderFacebook(UserRepository userRepository, TokenService tokenService) {
        super(userRepository, tokenService);
    }

    @Override
    public Token login(String token, String secret)  {
        try {
            Facebook facebook = new FacebookTemplate(token);
            String[] fields = {"id", "name", "birthday", "email", "location", "hometown", "gender", "first_name", "last_name"};

            org.springframework.social.facebook.api.User facebookUser = facebook.fetchObject("me",
                    org.springframework.social.facebook.api.User.class, fields);

            ObjectMapper mapper = new ObjectMapper();
            String fbUser = mapper.writeValueAsString(facebookUser);
            log.info("FB User: " + fbUser);

            String email = facebookUser.getEmail();

            log.info("FB User email: " + email);

            Token permToken = registerOrLogin(email);
            return Optional.of(permToken).orElseThrow(() -> new UnsupportedOperationException("Facebook auth fail"));
        } catch (Exception e) {
            throw new UnsupportedOperationException("Facebook auth fail");
        }
    }
}
