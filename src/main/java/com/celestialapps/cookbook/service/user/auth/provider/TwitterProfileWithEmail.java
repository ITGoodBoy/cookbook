package com.celestialapps.cookbook.service.user.auth.provider;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TwitterProfileWithEmail {

    @JsonProperty("email")
    private String email;
}
