package com.celestialapps.cookbook.service.user.auth.provider;

import com.celestialapps.cookbook.service.user.UserRepository;
import com.celestialapps.cookbook.service.user.auth.token.Token;
import com.celestialapps.cookbook.service.user.auth.token.TokenService;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;

import java.util.Collections;
import java.util.Optional;

class ProviderGoogle extends Provider {

    private final String appIdGoogle = "973517026712-a3et6t61mnrb9d639o64af547149i35v.apps.googleusercontent.com";
    private final String appSecret = "qVtPKbRApLfJLewdI1y48LTL";

    ProviderGoogle(UserRepository userRepository, TokenService tokenService) {
        super(userRepository, tokenService);
    }

    @Override
    public Token login(String token, String secret) {
        try {
            JacksonFactory jacksonFactory = new JacksonFactory();
            HttpTransport transport = new NetHttpTransport();

            GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jacksonFactory)
                    .setAudience(Collections.singletonList(appIdGoogle))
                    .build();

            GoogleIdToken idToken = verifier.verify(token);
            GoogleIdToken.Payload payload = idToken.getPayload();

            String email = payload.getEmail();
            Token permToken = registerOrLogin(email);

            return Optional.of(permToken).orElseThrow(() -> new UnsupportedOperationException("Google auth fail"));
        } catch (Exception e) {
            throw new UnsupportedOperationException("Google auth fail", e);
        }
    }
}
