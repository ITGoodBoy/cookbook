package com.celestialapps.cookbook.service.user;

import com.celestialapps.cookbook.exception.ServiceException;
import com.celestialapps.cookbook.request.SignInRequest;
import com.celestialapps.cookbook.request.SignUpConfirmRequest;
import com.celestialapps.cookbook.request.SignUpRequest;
import com.celestialapps.cookbook.service.user.auth.Credentials;
import com.celestialapps.cookbook.service.user.auth.provider.Provider;
import com.celestialapps.cookbook.service.user.auth.token.Token;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User findById(long id) throws ServiceException;
    Token login(SignInRequest signInRequest) throws ServiceException;
    Token registration(SignUpRequest signUpRequest) throws ServiceException;
    Token registrationConfirm(SignUpConfirmRequest signUpConfirmRequest) throws ServiceException;
    void restorePassword(String email) throws ServiceException;
    void changePassword(Credentials.ResetPassword credentials) throws ServiceException;
    void changePassword(Credentials.SetNewPassword credentials, User user) throws ServiceException;
    Token socialLogin(String token, String secret, Provider.Type type) throws ServiceException;

    User getLoggedUser();

    void save(User user) throws ServiceException;


}
