package com.celestialapps.cookbook.service.user.auth;

import lombok.AllArgsConstructor;
import lombok.Data;

public class Credentials {

    @Data
    @AllArgsConstructor
    public static class SignIn{
        private String login;
        private String password;
    }

    @Data
    @AllArgsConstructor
    public static class SignUp{
        private String login;
        private String email;
        private String password;
    }

    @Data
    @AllArgsConstructor
    public static class SignUpConfirm{
        private String email;
        private String code;
    }

    @Data
    @AllArgsConstructor
    public static class ResetPassword {
        private String code;
        private String email;
        private String password;
    }

    @Data
    @AllArgsConstructor
    public static class SetNewPassword {
        private String oldPassword;
        private String newPassword;
        private String repeatPassword;

    }

}
