package com.celestialapps.cookbook.service.user.temp;

import org.springframework.data.repository.CrudRepository;

public interface TempDataRepository extends CrudRepository<TempData, Long> {


    TempData findByEmail(String email);
    boolean existsByEmail(String email);
    boolean existsByEmailAndCode(String email, String code);
}
