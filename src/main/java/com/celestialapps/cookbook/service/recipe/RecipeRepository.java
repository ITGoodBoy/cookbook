package com.celestialapps.cookbook.service.recipe;

import com.celestialapps.cookbook.response.CookingAlgorithmResponse;
import com.celestialapps.cookbook.response.IngredientResponse;
import com.celestialapps.cookbook.response.RecipeResponse;
import com.celestialapps.cookbook.service.cooking_algorithm.CookingAlgorithm;
import com.celestialapps.cookbook.service.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Repository
public interface RecipeRepository extends PagingAndSortingRepository<Recipe, Long> {


    @Query("select new com.celestialapps.cookbook.response.RecipeResponse" +
            "(r.category.id, r.id, r.videoId, r.likeCount, r.comments.size, " +
            "r.name, r.user.login, r.date, r.imgUrl, r.isVegetarian, r.isVegan, r.isPublished)" +
            " from Recipe r")
    List<RecipeResponse> findAllRecipeResponses(Pageable pageable);

    @Query("select new com.celestialapps.cookbook.response.RecipeResponse" +
            "(r.category.id, r.id, r.videoId, r.likeCount, r.comments.size, " +
            "r.name, r.user.login, r.date, r.imgUrl, r.isVegetarian, r.isVegan, r.isPublished)" +
            " from Recipe r")
    List<RecipeResponse> findAllRecipeResponses();

    @Query("select new com.celestialapps.cookbook.response.IngredientResponse" +
            "(r.id, i.id, i.ingredientShort.name, i.ingredientShort.imgUrl)" +
            " from Recipe r inner join r.ingredients i where r.id in :ids"  )
    List<IngredientResponse> findAllIngredientResponses(@Param("ids") Set<Long> ids);

    @Query("select new com.celestialapps.cookbook.response.CookingAlgorithmResponse" +
            "(r.id, c.id, c.text, c.imgUrl)" +
            " from Recipe r inner join r.cookingAlgorithms c where r.id in :ids"  )
    List<CookingAlgorithmResponse> findAllCookingAlgorithm(@Param("ids") Set<Long> ids);

    List<Recipe> findByUser(User user);

    Recipe findByIdAndUser(long id, User user);

}
