package com.celestialapps.cookbook.service.recipe;

import com.celestialapps.cookbook.exception.ServiceException;
import com.celestialapps.cookbook.request.RecipeRequest;
import com.celestialapps.cookbook.response.RecipeFullResponse;
import com.celestialapps.cookbook.response.RecipeResponse;
import com.celestialapps.cookbook.service.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface RecipeService {


    Recipe getRecipe(long id) throws ServiceException;
    Page<Recipe> getRecipes(Pageable pageable) throws ServiceException;
    List<RecipeResponse> getRecipeResponses(Pageable pageable) throws ServiceException;
    List<RecipeResponse> getAllRecipeResponses() throws ServiceException;

    RecipeFullResponse createRecipe(User user, RecipeRequest recipeRequest) throws ServiceException;
    RecipeFullResponse updateRecipe(long recipeId, User user, RecipeRequest recipeRequest) throws ServiceException;
    List<RecipeFullResponse> getMyRecipes(User user);
}
