package com.celestialapps.cookbook.service.recipe;

import com.celestialapps.cookbook.autogenerate.MessageResources;
import com.celestialapps.cookbook.exception.ServiceException;
import com.celestialapps.cookbook.config.language.MessageData;
import com.celestialapps.cookbook.request.CookingAlgorithmRequest;
import com.celestialapps.cookbook.request.IngredientRequest;
import com.celestialapps.cookbook.request.RecipeRequest;
import com.celestialapps.cookbook.response.CookingAlgorithmResponse;
import com.celestialapps.cookbook.response.IngredientResponse;
import com.celestialapps.cookbook.response.RecipeFullResponse;
import com.celestialapps.cookbook.response.RecipeResponse;
import com.celestialapps.cookbook.service.category.CategoryRepository;
import com.celestialapps.cookbook.service.cooking_algorithm.CookingAlgorithm;
import com.celestialapps.cookbook.service.cooking_algorithm.CookingAlgorithmRepository;
import com.celestialapps.cookbook.service.ingredient.IngredientShort;
import com.celestialapps.cookbook.service.ingredient.IngredientShortRepository;
import com.celestialapps.cookbook.service.ingredient.ingredient.Ingredient;
import com.celestialapps.cookbook.service.ingredient.ingredient.IngredientRepository;
import com.celestialapps.cookbook.service.user.User;
import com.celestialapps.cookbook.service.user.UserService;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
public class RecipeServiceImpl implements RecipeService {

    private RecipeRepository recipeRepository;
    private CategoryRepository categoryRepository;
    private CookingAlgorithmRepository cookingAlgorithmRepository;
    private IngredientShortRepository ingredientShortRepository;
    private IngredientRepository ingredientRepository;
    private UserService userService;
    private MessageData messageData;

    @Autowired
    public RecipeServiceImpl(RecipeRepository recipeRepository, CategoryRepository categoryRepository,
                             CookingAlgorithmRepository cookingAlgorithmRepository,
                             IngredientShortRepository ingredientShortRepository,
                             IngredientRepository ingredientRepository,
                             UserService userService, MessageData messageData) {

        this.recipeRepository = recipeRepository;
        this.categoryRepository = categoryRepository;
        this.cookingAlgorithmRepository = cookingAlgorithmRepository;
        this.ingredientShortRepository = ingredientShortRepository;
        this.ingredientRepository = ingredientRepository;
        this.userService = userService;
        this.messageData = messageData;
    }

    @Override
    public Recipe getRecipe(long id) throws ServiceException {
        Recipe recipe = recipeRepository.findById(id).orElse(null);
        if (recipe == null) throw new ServiceException(messageData.getErrorMessage(MessageResources.RECIPE_NOT_FOUND));

        return recipe;
    }


    @Override
    public Page<Recipe> getRecipes(Pageable pageable) throws ServiceException {
        return recipeRepository.findAll(pageable);
    }

    @Override
    public List<RecipeResponse> getRecipeResponses(Pageable pageable) throws ServiceException {
        Map<Long, RecipeResponse> recipeResponses = recipeRepository
                .findAllRecipeResponses(pageable)
                .stream()
                .collect(Collectors.toMap(RecipeResponse::getId, recipeResponse -> recipeResponse));

        Multimap<Long, IngredientResponse> ingredientResponses = recipeRepository
                .findAllIngredientResponses(recipeResponses.keySet())
                .stream()
                .collect(Multimaps.toMultimap(
                        IngredientResponse::getRecipeId,
                        ingredientResponse -> ingredientResponse,
                        (Supplier<Multimap<Long, IngredientResponse>>) ArrayListMultimap::create));

        recipeResponses
                .values()
                .forEach(recipeResponse -> recipeResponse
                        .getIngredientResponses()
                        .addAll(ingredientResponses.get(recipeResponse.getId())));

        return new ArrayList<>(recipeResponses.values());
    }

    @Override
    public List<RecipeResponse> getAllRecipeResponses() throws ServiceException {
        Map<Long, RecipeResponse> recipeResponses = recipeRepository
                .findAllRecipeResponses()
                .stream()
                .collect(Collectors.toMap(RecipeResponse::getId, recipeResponse -> recipeResponse));

        Multimap<Long, IngredientResponse> ingredientResponses = recipeRepository
                .findAllIngredientResponses(recipeResponses.keySet())
                .stream()
                .collect(Multimaps.toMultimap(
                        IngredientResponse::getRecipeId,
                        ingredientResponse -> ingredientResponse,
                        (Supplier<Multimap<Long, IngredientResponse>>) ArrayListMultimap::create));

        Multimap<Long, CookingAlgorithmResponse> cookingAlgorithms = recipeRepository
                .findAllCookingAlgorithm(recipeResponses.keySet())
                .stream()
                .collect(Multimaps.toMultimap(
                        CookingAlgorithmResponse::getRecipeId,
                        cookingAlgorithmResponse -> cookingAlgorithmResponse,
                        (Supplier<Multimap<Long, CookingAlgorithmResponse>>) ArrayListMultimap::create));

        recipeResponses
                .values()
                .forEach(recipeResponse -> {
                    recipeResponse
                            .getIngredientResponses()
                            .addAll(ingredientResponses.get(recipeResponse.getId()));

                    recipeResponse
                            .getCookingAlgorithmResponses()
                            .addAll(cookingAlgorithms.get(recipeResponse.getId()));
                });

        return new ArrayList<>(recipeResponses.values());
    }

    @Transactional
    @SuppressWarnings("Duplicates")
    @Override
    public RecipeFullResponse createRecipe(User user, RecipeRequest recipeRequest) throws ServiceException {
        if (!categoryRepository.existsByName(recipeRequest.getCategoryName())) {
            throw new ServiceException("Категория не найдена");
        }

        List<IngredientRequest> ingredientRequests = recipeRequest.getIngredientRequests();

        Set<String> ingredientRequestNames = ingredientRequests
                .stream().map(IngredientRequest::getName).collect(Collectors.toSet());

        Set<IngredientShort> ingredientShorts = ingredientShortRepository
                .findAllByNameIn(ingredientRequestNames);

        List<Ingredient> ingredients = new ArrayList<>();

        for (IngredientShort ingredientShort : ingredientShorts) {
            String ingredientShortName = ingredientShort.getName();

            for (IngredientRequest ingredientRequest : ingredientRequests) {
                if (ingredientShortName.equals(ingredientRequest.getName())) {
                    Ingredient ingredient = new Ingredient(ingredientShort, ingredientRequest.getCount());
                    ingredients.add(ingredient);
                }
            }
        }

        List<CookingAlgorithm> cookingAlgorithms = recipeRequest
                .getCookingAlgorithmRequests()
                .stream()
                .map(cook -> new CookingAlgorithm(cook.getDescription(), cook.getImgUrl()))
                .collect(Collectors.toList());

        ingredients = (List<Ingredient>) ingredientRepository.saveAll(ingredients);
        cookingAlgorithms = (List<CookingAlgorithm>) cookingAlgorithmRepository.saveAll(cookingAlgorithms);

        Recipe recipe = new Recipe(recipeRequest.getName(), recipeRequest.getVideoId(), cookingAlgorithms,
                recipeRequest.getImgUrl(), categoryRepository.findByName(recipeRequest.getCategoryName()), user, ingredients);

        recipe = recipeRepository.save(recipe);


        return new RecipeFullResponse(recipe);
    }
    @Transactional
    @SuppressWarnings("Duplicates")
    @Override
    public RecipeFullResponse updateRecipe(long recipeId, User user, RecipeRequest recipeRequest) throws ServiceException {
        Recipe recipe = recipeRepository.findByIdAndUser(recipeId, user);

        if (recipe == null) throw new ServiceException("recipe not found");

        if (!categoryRepository.existsByName(recipeRequest.getCategoryName())) {
            throw new ServiceException("Категория не найдена");
        }

        List<IngredientRequest> ingredientRequests = recipeRequest.getIngredientRequests();

        Set<String> ingredientRequestNames = ingredientRequests
                .stream().map(IngredientRequest::getName).collect(Collectors.toSet());

        Set<IngredientShort> ingredientShorts = ingredientShortRepository
                .findAllByNameIn(ingredientRequestNames);

        List<Ingredient> ingredients = new ArrayList<>();

        for (IngredientShort ingredientShort : ingredientShorts) {
            String ingredientShortName = ingredientShort.getName();

            for (IngredientRequest ingredientRequest : ingredientRequests) {
                if (ingredientShortName.equals(ingredientRequest.getName())) {
                    Ingredient ingredient = new Ingredient(ingredientShort, ingredientRequest.getCount());
                    ingredients.add(ingredient);
                }
            }
        }

        List<CookingAlgorithm> cookingAlgorithms = recipeRequest
                .getCookingAlgorithmRequests()
                .stream()
                .map(cook -> new CookingAlgorithm(cook.getDescription(), cook.getImgUrl()))
                .collect(Collectors.toList());


        ingredients = (List<Ingredient>) ingredientRepository.saveAll(ingredients);
        cookingAlgorithms = (List<CookingAlgorithm>) cookingAlgorithmRepository.saveAll(cookingAlgorithms);

        recipe.getIngredients().clear();
        recipe.getCookingAlgorithms().clear();

        recipe.setCategory(categoryRepository.findByName(recipeRequest.getCategoryName()));
        recipe.getIngredients().addAll(ingredients);
        recipe.getCookingAlgorithms().addAll(cookingAlgorithms);

        recipe = recipeRepository.save(recipe);

        return new RecipeFullResponse(recipe);
    }

    @Override
    public List<RecipeFullResponse> getMyRecipes(User user) {

        List<RecipeFullResponse> recipeFullResponses = new ArrayList<>();

        for (Recipe recipe : recipeRepository.findByUser(user)) {
            recipeFullResponses.add(new RecipeFullResponse(recipe));
        }

        return recipeFullResponses;
    }
}
