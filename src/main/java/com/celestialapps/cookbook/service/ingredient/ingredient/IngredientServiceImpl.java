package com.celestialapps.cookbook.service.ingredient.ingredient;

import com.celestialapps.cookbook.autogenerate.MessageResources;
import com.celestialapps.cookbook.exception.ServiceException;
import com.celestialapps.cookbook.config.language.MessageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientServiceImpl implements IngredientService{

    private IngredientRepository ingredientRepository;
    private MessageData messageData;

    @Autowired
    public IngredientServiceImpl(IngredientRepository ingredientRepository, MessageData messageData) {
        this.ingredientRepository = ingredientRepository;
        this.messageData = messageData;
    }

    @Override
    public Ingredient getIngredient(long id) throws ServiceException {
        Ingredient ingredient = ingredientRepository.findById(id).orElse(null);

        if (ingredient == null) throw new ServiceException(messageData.getErrorMessage(MessageResources.INGREDIENT_NOT_FOUND));

        return ingredient;
    }

    @Override
    public Ingredient getIngredient(String name) throws ServiceException {
       // Ingredient ingredient = ingredientRepository.findByName(name);

       // if (ingredient == null) throw new ServiceException(messageData.getErrorMessage(MessageResources.INGREDIENT_NOT_FOUND));

        return null;
    }

    @Override
    public List<Ingredient> getIngredients() throws ServiceException {
        return ingredientRepository.findAll();
    }
}
