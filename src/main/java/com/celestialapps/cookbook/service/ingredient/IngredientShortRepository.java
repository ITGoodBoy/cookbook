package com.celestialapps.cookbook.service.ingredient;

import com.celestialapps.cookbook.response.IngredientResponse;
import com.celestialapps.cookbook.response.IngredientShortResponse;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface IngredientShortRepository extends PagingAndSortingRepository<IngredientShort, Long>{


    @Query("select new com.celestialapps.cookbook.response.IngredientShortResponse" +
            "(i.id, i.name, i.imgUrl)" +
            " from IngredientShort i"  )
    List<IngredientShortResponse> findAllIngredientShortResponses();

    Set<IngredientShort> findAllByNameIn(Set<String> names);
}
