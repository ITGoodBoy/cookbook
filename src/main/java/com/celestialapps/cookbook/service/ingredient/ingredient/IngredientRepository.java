package com.celestialapps.cookbook.service.ingredient.ingredient;

import com.celestialapps.cookbook.response.IngredientResponse;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IngredientRepository extends PagingAndSortingRepository<Ingredient, Long> {


    @Override
    List<Ingredient> findAll();

}
