package com.celestialapps.cookbook.service.ingredient;

import com.celestialapps.cookbook.response.IngredientShortResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientShortServiceImpl implements IngredientShortService{

    private IngredientShortRepository ingredientShortRepository;

    @Autowired
    public IngredientShortServiceImpl(IngredientShortRepository ingredientShortRepository) {
        this.ingredientShortRepository = ingredientShortRepository;
    }

    @Override
    public List<IngredientShortResponse> getAllIngredientShortResponses() {
        return ingredientShortRepository.findAllIngredientShortResponses();
    }
}
