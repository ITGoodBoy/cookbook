package com.celestialapps.cookbook.service.email;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
public class EmailSender {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    private String userName;
    private String password;
    private String host;
    private int port;
    private String protocol;
    private int sendCount;
    private Date createAt = new Date(System.currentTimeMillis());

    public EmailSender(String userName, String host, String password, int port, String protocol) {
        this.userName = userName;
        this.host = host;
        this.password = password;
        this.port = port;
        this.protocol = protocol;
    }

    public void increaseSendCount() {
        ++sendCount;
    }
}



//    JavaMailSenderImpl sender = new JavaMailSenderImpl();
//            sender.setHost("smtp.gmail.com");
//                    sender.setPassword("1qaz1qaz1C");
//                    sender.setPort(465);
//                    sender.setProtocol("smtps");
//
//                    Properties properties = new Properties();
//                    properties.setProperty("spring.mail.properties.mail.smtp.auth", "true");
//                    properties.setProperty("spring.mail.properties.mail.smtp.starttls.enable", "true");
//
//                    sender.setJavaMailProperties(properties);
//
//                    javaMailSenders.add(sender);
//                    }
//
//                    javaMailSenders.get(0).setUsername("cookbookemailsender@gmail.com");
//                    javaMailSenders.get(1).setUsername("cookbookemailsender2@gmail.com");