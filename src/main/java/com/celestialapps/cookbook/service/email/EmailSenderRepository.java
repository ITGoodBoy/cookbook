package com.celestialapps.cookbook.service.email;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EmailSenderRepository extends PagingAndSortingRepository<EmailSender, Long> {


    EmailSender findByUserName(String userName);
}
