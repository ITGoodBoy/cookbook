package com.celestialapps.cookbook.service.category;

import com.celestialapps.cookbook.service.recipe.Recipe;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    @Column(unique = true)
    private String name;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "category")
    @JsonManagedReference
    private List<Recipe> recipes;
    @Column(unique = true, length = 550)
    private String imgUrl;

    public Category(String name, List<Recipe> recipes, String imgUrl) {
        this.name = name;
        this.recipes = recipes;
        this.imgUrl = imgUrl;
    }
}
