package com.celestialapps.cookbook.service.category;

import com.celestialapps.cookbook.response.CategoryResponse;

import java.util.List;

public interface CategoryService {

    List<CategoryResponse> getAllCategoryResponses();
}
