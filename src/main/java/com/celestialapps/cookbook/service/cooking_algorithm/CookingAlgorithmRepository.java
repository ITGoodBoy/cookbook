package com.celestialapps.cookbook.service.cooking_algorithm;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface CookingAlgorithmRepository extends PagingAndSortingRepository<CookingAlgorithm, Long> {
}
