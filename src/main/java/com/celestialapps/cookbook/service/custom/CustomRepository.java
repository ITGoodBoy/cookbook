package com.celestialapps.cookbook.service.custom;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomRepository extends CrudRepository<Custom, Long> {


    @Query(value = "SELECT sum( data_length + index_length ) / 1024 / 1024 as DataBase_Size_in_MB FROM information_schema.TABLES where table_schema='cookbook'", nativeQuery = true)
    long getDatabaseSumInMB();
}
