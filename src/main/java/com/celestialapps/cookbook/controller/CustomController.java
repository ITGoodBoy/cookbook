package com.celestialapps.cookbook.controller;

import com.celestialapps.cookbook.exception.ServiceException;
import com.celestialapps.cookbook.response.AllDataResponse;
import com.celestialapps.cookbook.service.category.CategoryService;
import com.celestialapps.cookbook.service.custom.CustomService;
import com.celestialapps.cookbook.service.ingredient.IngredientShort;
import com.celestialapps.cookbook.service.ingredient.IngredientShortService;
import com.celestialapps.cookbook.service.ingredient.ingredient.IngredientService;
import com.celestialapps.cookbook.service.recipe.RecipeService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/custom", headers = HttpHeaders.ACCEPT_LANGUAGE)
@Api(value = "Custom API Module")
public class CustomController {

    private CustomService customService;
    private CategoryService categoryService;
    private RecipeService recipeService;
    private IngredientShortService ingredientShortService;

    @Autowired
    public CustomController(CustomService customService, CategoryService categoryService,
                            RecipeService recipeService, IngredientShortService ingredientShortService) {
        this.customService = customService;
        this.categoryService = categoryService;
        this.recipeService = recipeService;
        this.ingredientShortService = ingredientShortService;
    }

    @GetMapping("/current-database-size-in-mb")
    @ResponseBody
    public long getCurrentDatabaseSizeInMB() {
        return customService.getCurrentDatabaseSizeInMB();
    }

    @GetMapping("/all-data")
    public AllDataResponse getAllDataResponse() throws ServiceException {
        AllDataResponse allDataResponse = new AllDataResponse();

        allDataResponse.setRecipeResponses(recipeService.getAllRecipeResponses());
        allDataResponse.setCategoryResponses(categoryService.getAllCategoryResponses());
        allDataResponse.setIngredientShortResponses(ingredientShortService.getAllIngredientShortResponses());

        return allDataResponse;
    }
}
