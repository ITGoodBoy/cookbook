package com.celestialapps.cookbook.controller;

import com.celestialapps.cookbook.constants.HeaderConstants;
import com.celestialapps.cookbook.exception.ServiceException;
import com.celestialapps.cookbook.request.RecipeRequest;
import com.celestialapps.cookbook.response.RecipeFullResponse;
import com.celestialapps.cookbook.response.RecipeResponse;
import com.celestialapps.cookbook.service.recipe.Recipe;
import com.celestialapps.cookbook.service.recipe.RecipeRepository;
import com.celestialapps.cookbook.service.recipe.RecipeService;
import com.celestialapps.cookbook.service.user.User;
import com.celestialapps.cookbook.service.user.UserService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/recipes", headers = {HttpHeaders.ACCEPT_LANGUAGE})
@Api(value = "Ingredient API Module", description = "getRecipe, getRecipes")
public class RecipeController{

    private RecipeService recipeService;
    private UserService userService;
    private RecipeRepository recipeRepository;

    @Autowired
    public RecipeController(RecipeService recipeService, UserService userService, RecipeRepository recipeRepository) {
        this.recipeService = recipeService;
        this.userService = userService;
        this.recipeRepository = recipeRepository;
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Recipe getRecipe(@PathVariable("id") long id) throws ServiceException {
        return recipeService.getRecipe(id);
    }

    @GetMapping("/")
    @ResponseBody
    public Page<Recipe> getRecipes(Pageable pageable) throws ServiceException {
        return recipeRepository.findAll(pageable);
    }

    @GetMapping("/recipe-shorts")
    public List<RecipeResponse> getRecipeShorts(Pageable pageable) throws ServiceException {
        return recipeService.getRecipeResponses(pageable);
    }

    @PostMapping(value = "/create-recipe", headers = HeaderConstants.HEADER_TOKEN)
    public ResponseEntity<RecipeFullResponse> createRecipe(@RequestBody RecipeRequest recipeRequest) throws ServiceException {
        User user = userService.getLoggedUser();
        return ResponseEntity.ok(recipeService.createRecipe(user, recipeRequest));
    }

    @PutMapping(value = "/update-recipe", headers = HeaderConstants.HEADER_TOKEN)
    public ResponseEntity<RecipeFullResponse> updateRecipe(@RequestParam long id, @RequestBody RecipeRequest recipeRequest) throws ServiceException {
        User user = userService.getLoggedUser();
        return ResponseEntity.ok(recipeService.updateRecipe(id, user, recipeRequest));
    }

    @GetMapping(value = "/get-my-recipes", headers = HeaderConstants.HEADER_TOKEN)
    public ResponseEntity<List<RecipeFullResponse>> getMyRecipes() throws ServiceException {
        User user = userService.getLoggedUser();
        return ResponseEntity.ok(recipeService.getMyRecipes(user));
    }

}
