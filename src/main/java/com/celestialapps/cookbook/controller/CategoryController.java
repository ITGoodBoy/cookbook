package com.celestialapps.cookbook.controller;

import com.celestialapps.cookbook.response.CategoryResponse;
import com.celestialapps.cookbook.service.category.CategoryService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/category", headers = HttpHeaders.ACCEPT_LANGUAGE)
@Api(value = "Category API Module", description = "getCategoryShorts")
public class CategoryController {

    private CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/category-shorts")
    @ResponseBody
    public List<CategoryResponse> getCategoryShorts() {
        return categoryService.getAllCategoryResponses();
    }
}
